Introduction
============

The collective.searchandreplace product is a Plone Add-on designed to find and replace text in Plone content objects, namely titles, descriptions, and document text. It operates over single or multiple Plone content objects and can preview as well as actuate changes.

Optional features include being able to control searching in subfolders, and matching based on case sensatvity/insensativity.

New in version 2.0, product has been completely refactored and brought up to date for use with Plone 4.

The collective.searchandreplace product was initally built for use with eduCommons by Novell, inc. It is currently maintained by enPraxis (http://enpraxis.net).







